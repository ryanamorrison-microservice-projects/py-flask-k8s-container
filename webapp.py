from flask import Flask, render_template
import os
app = Flask(__name__)

@app.get("/")
def index():
    return render_template(
        "index.html",
        title="system info",
        pod_name=os.environ['POD_NAME'],
        pod_ns=os.environ['POD_NS'],
        pod_ip=os.environ['POD_IP'],
        node_name=os.environ['NODE_NAME'],
    )

if __name__ == "__main__":
    app.run()