import pytest
from webapp import app

def test_app(monkeypatch: pytest.MonkeyPatch):
    monkeypatch.setenv("POD_NAME","webapp-7848d6c4d5-szd4h")
    monkeypatch.setenv("POD_NS","default")
    monkeypatch.setenv("POD_IP","192.168.200.1")
    monkeypatch.setenv("NODE_NAME","worker01")
    response = app.test_client().get('/')

    assert response.status_code == 200
    assert b"webapp" in response.data
    assert b"default" in response.data
    assert b"192.168.200.1" in response.data
    assert b"worker01" in response.data

def test_app_no_data():
    response = app.test_client().get('/')
    assert b"webapp" not in response.data
    assert b"default" not in response.data
    assert b"192.168.200.1" not in response.data
    assert b"worker01" not in response.data