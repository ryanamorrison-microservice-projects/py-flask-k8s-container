py-flask-k8s-container
=========

A basic "front-end" microservice written in the python Flask framework that returns some environmental variables from kubernetes. It is intended to be a proof-of-concept.

Dependencies
------------
See requirements.txt

License
-------

BSD

Author Information
------------------

Ryan A. Morrison (ryan@ryanamorrison.com)

