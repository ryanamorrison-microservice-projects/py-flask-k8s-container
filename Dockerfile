FROM python:3.10-slim
COPY requirements.txt /
COPY gunicorn_start.sh /
RUN pip3 install -r /requirements.txt
RUN mkdir -p /app/{templates,static}
COPY templates/* /app/templates/
#COPY static/* /app/static/
COPY gunicorn_config.py /app
COPY webapp.py /app

WORKDIR /app
ENTRYPOINT ["/gunicorn_start.sh"]